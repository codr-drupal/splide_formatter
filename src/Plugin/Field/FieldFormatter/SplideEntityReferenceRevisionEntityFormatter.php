<?php

declare(strict_types=1);

namespace Drupal\splide_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsEntityFormatter;

/**
 * Plugin implementation of the 'Splide' formatter.
 *
 * @FieldFormatter(
 *   id = "splide_entity_reference_revisions_formatter",
 *   label = @Translation("Splide Rendered Entity"),
 *   field_types = {"entity_reference_revisions"},
 * )
 */
class SplideEntityReferenceRevisionEntityFormatter extends EntityReferenceRevisionsEntityFormatter {
  use SplideFormatterTrait;
}
