<?php

declare(strict_types=1);

namespace Drupal\splide_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\splide_formatter\Splide\SplideOption;

trait SplideFormatterTrait {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $splide_configuration = [];

    foreach(SplideOption::load() as $option) {
      $splide_configuration[$option->name] = $option->default;
    }

    return [
      'splide_configuration' => $splide_configuration,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);

    $splide_configuration = $this->getSetting('splide_configuration');

    $elements['splide_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Splide configuration'),
      '#weight' => 9,
      '#description' => 'https://splidejs.com/guides/options/#options',
    ];

    foreach(SplideOption::load() as $option) {
      $elements['splide_configuration'][$option->name] = [
        '#title' => $option->name,
        '#default_value' => $splide_configuration[$option->name] ?? '',
        '#description' => $option->description,
      ];

      $elements['splide_configuration'][$option->name]['#type'] = match ($option->type) {
        SplideOption::STRING => 'textfield',
        SplideOption::NUMBER => 'number',
        SplideOption::BOOLEAN => 'checkbox',
        SplideOption::LIST => 'select',
      };

      if($option->type === SplideOption::LIST) {
        $elements['splide_configuration'][$option->name]['#options'] = array_combine($option->options, $option->options);
      }

      if(!empty($option->states)) {
        $states = [];

        foreach($option->states as $state => $conditions) {
          $states[$state] = [];
          foreach ($conditions as $field => $condition) {
            if(is_integer($field)) {
            $states[$state][] = $condition;
            }
            else {
              $states[$state]['[name$="[splide_configuration][' . $field . ']"]'] = $condition;
            }
          }
        }
        $elements['splide_configuration'][$option->name]['#states'] = $states;
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $splide_configuration = $this->getSetting('splide_configuration');

    $splide_options = SplideOption::load();

    $configuration = [];
    foreach($splide_configuration as $name => $value) {
      // Check option exists
      if (!isset($splide_options[$name])) {
        continue;
      }

      $option = $splide_options[$name];

      // Do not inject default values
      if ($value === $option->default) {
        continue;
      }

      // Do not inject empty values
      if ($value === '') {
        continue;
      }

      // Transform boolean values
      if ($option->type === SplideOption::BOOLEAN) {
        $configuration[$name] = (bool) $value;
        continue;
      }

      // classic value
      $configuration[$name] = $value;
    }

    return [
      '#theme' => 'splide_formatter__splide',
      '#slides' => parent::viewElements($items, $langcode),
      '#configuration' => $configuration,
      '#attached' => [
        'library' => ['splide_formatter/slideshow']
      ],
    ];
  }
}
