<?php

declare(strict_types=1);

namespace Drupal\splide_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'Splide' formatter.
 *
 * @FieldFormatter(
 *   id = "splide_image_formatter",
 *   label = @Translation("Splide Image"),
 *   field_types = {"image"},
 * )
 */
class SplideImageFormatter extends ImageFormatter {
  use SplideFormatterTrait;
}
