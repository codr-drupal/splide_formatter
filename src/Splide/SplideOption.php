<?php

declare(strict_types=1);

namespace Drupal\splide_formatter\Splide;

final class SplideOption {
  public const BOOLEAN = 'boolean';
  public const STRING = 'string';
  public const NUMBER = 'number';
  public const LIST = 'list';

  public function __construct(
    readonly string $name,
    readonly string $description,
    readonly string $type,
    readonly string|bool|int $default,
    readonly array $options = [],
    readonly array $states = [],
  ) {}

  /**
   * @return self[]
   */
  static function load(): array {
//    ['visible' => ['type' => ['value' => 'loop']]]
    $options = [
      new self('type', 'The type of the carousel. To loop the fade carousel, enable the rewind option.', self::LIST, 'slide', ['slide', 'loop', 'fade']),
      new self('role', 'Sets a role attribute to the root element. It must be a landmark role or group.', self::STRING, 'region'),
      new self('label', 'Sets an aria-label attribute to the root element. This option overwrites the attribute defined by HTML.', self::STRING, ''),
      new self('labelledby', 'Sets an aria-labelledby attribute to the root element. This option overwrites the attribute defined by HTML.', self::STRING, ''),
      new self('rewind', 'Determines whether to rewind the carousel or not. This does not work in the loop mode.', self::BOOLEAN, false, [], ['invisible' => ['type' => ['value' => 'loop']]]),
      new self('speed', 'The transition speed in milliseconds. If 0, the carousel immediately jumps to the target slide.', self::NUMBER, 400),
      new self('rewindSpeed', 'The transition speed on rewind in milliseconds. The speed value is used as default.', self::NUMBER, ''),
      new self('rewindByDrag', 'Allows users to rewind a carousel by drag. The rewind option must be true.', self::BOOLEAN, false),
      new self('width', 'Defines the carousel max width, accepting the CSS format such as 10em, 80vw.', self::STRING, ''),
      new self('height', 'Defines the slide height, accepting the CSS format except for %.', self::STRING, '', [], ['invisible' => ['fixedHeight' => ['filled' => TRUE]]]),
      new self('fixedWidth', 'Fixes width of slides, accepting the CSS format. The carousel will ignore the perPage option if you provide this value.', self::STRING, ''),
      new self('fixedHeight', 'Fixes height of slides, accepting the CSS format except for %. The carousel will ignore perPage, height and heightRatio options if you provide this value.', self::STRING, ''),
      new self('heightRatio', 'Determines height of slides by the ratio to the carousel width.', self::STRING, '', [], ['invisible' => ['fixedHeight' => ['filled' => TRUE]]]),
      new self('autoWidth', 'If true, the width of slides are determined by their width. Do not provide perPage and perMove options (or set them to 1).', self::BOOLEAN, false),
      new self('autoHeight', 'If true, the height of slides are determined by their height. Do not provide perPage and perMove options (or set them to 1).', self::BOOLEAN, false),
      new self('start', 'Defines the start index.', self::NUMBER, 0),
      new self('perPage', 'Determines the number of slides to display in a page. The value must be an integer.', self::NUMBER, 1, [], ['invisible' => ['fixedWidth' => ['filled' => TRUE], 'fixedHeight' => ['filled' => TRUE]]]),
      new self('perMove', 'Determines the number of slides to move at once. The following example displays 3 slides per page but moves slides one by one. The value must be an integer.', self::NUMBER, 1),
      new self('gap', 'The gap between slides. The CSS format is acceptable, such as 1em.', self::STRING, ''),
      new self('padding', 'Sets padding left/right for the horizontal carousel or top/bottom for the vertical carousel. ', self::STRING, ''),
      new self('arrow', 'Determines whether to create arrows or not.', self::BOOLEAN, true),
      new self('pagination', 'Determines whether to create pagination (indicator dots) or not.', self::BOOLEAN, true),
      new self('paginationKeyboard', 'Determines whether to enable keyboard shortcuts for pagination when it contains focus.', self::BOOLEAN, true),
      new self('drag', 'Determines whether to allow the user to drag the carousel or not.', self::BOOLEAN, true),
      new self('snap', 'Snaps the closest slide in the drag free mode.', self::BOOLEAN, false),
      new self('arrowPath', 'Changes the arrow SVG path, like `m7.61 0.807-2.12...`. The SVG size must be 40×40.', self::STRING, ''),
      new self('autoplay', 'Determines whether to enable autoplay or not', self::BOOLEAN, false),
      new self('interval', 'The autoplay interval duration in milliseconds.', self::NUMBER, 5000),
      new self('pauseOnHover', 'Determines whether to pause autoplay on mouseover. This should be true for accessibility.', self::BOOLEAN, true),
      new self('pauseOnFocus', 'Determines whether to pause autoplay while the carousel contains the active element (focused element). This should be true for accessibility.', self::BOOLEAN, true),
      new self('resetProgress', 'Determines whether to reset the autoplay progress when it is requested to start again.', self::BOOLEAN, true),
      new self('cover', 'Converts the image src to the css background-image URL of the parent element. This requires height, fixedHeight or heightRatio option.', self::BOOLEAN, false),/*, [], [
        'visible' => [
          'height' => ['filled' => TRUE],
          'or',
          'fixedHeight' => ['filled' => TRUE],
          'or',
          'heightRatio' => ['filled' => TRUE]
        ]]),*/
    ];

    return array_combine(
      array_map(function(SplideOption $item){ return $item->name; }, $options),
      $options,
    );
  }
}
