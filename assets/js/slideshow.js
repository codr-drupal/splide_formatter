(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.SplideFormatterSlideshow = {
    attach (context, settings) {
      once('SplideFormatterSlideshow', '.splide').forEach(element => {
        const main = new Splide(element);
        main.mount();
      })
    }
  };

} (Drupal, once));
